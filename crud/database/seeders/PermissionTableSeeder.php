<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
 
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'roles-index',
           'roles-create',
           'roles-store',
           'roles-edit',
           'roles-update',
           'roles-destroy',
          
        ];
     
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
       }
    }
}
//php artisan make:seeder PermissionTableSeeder