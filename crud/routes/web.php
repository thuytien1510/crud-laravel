<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('page.index');


Route::get('/404', function () {
    return view('errors.404');
})->name('page.404');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\UserController::class, 'index'])->name('users.index')->middleware('check.permission:users.index');
    Route::get('create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create')->middleware('check.permission:users.create');
    Route::post('store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store')->middleware('check.permission:users.store');
    Route::get('edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit')->middleware('check.permission:users.edit');
    Route::post('update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update')->middleware('check.permission:users.update');
    Route::post('delete/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy')->middleware('check.permission:users.destroy');




});
Route::group(['prefix' => 'role',  'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index')->middleware('check.permission:roles.index');
    Route::get('create', [App\Http\Controllers\RoleController::class, 'create'])->name('roles.create')->middleware('check.permission:roles.create');
    Route::post('store', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store')->middleware('check.permission:roles.store');
    Route::get('edit/{id}', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit')->middleware('check.permission:roles.edit');
    Route::post('update/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update')->middleware('check.permission:roles.update');
    Route::get('delete/{id}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy')->middleware('check.permission:roles.destroy');


});
Route::group(['prefix' => 'category', 'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\CategoryController::class, 'index'])->name('categories.index')->middleware('check.permission:categories.index');
    Route::get('create', [App\Http\Controllers\CategoryController::class, 'create'])->name('categories.create')->middleware('check.permission:categories.create');
    Route::post('store', [App\Http\Controllers\CategoryController::class, 'store'])->name('categories.store')->middleware('check.permission:categories.store');
    Route::get('edit/{id}', [App\Http\Controllers\CategoryController::class, 'edit'])->name('categories.edit')->middleware('check.permission:categories.edit');
    Route::post('update/{id}', [App\Http\Controllers\CategoryController::class, 'update'])->name('categories.update')->middleware('check.permission:categories.update');
    Route::get('delete/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->name('categories.destroy')->middleware('check.permission:categories.destroy');


});
Route::group(['prefix' => 'product', 'middleware' => 'auth'], function () {

    Route::get('', [App\Http\Controllers\ProductController::class, 'index'])->name('products.index')->middleware('check.permission:products.index');
    Route::get('create', [App\Http\Controllers\ProductController::class, 'create'])->name('products.create')->middleware('check.permission:products.create');
    Route::post('store', [App\Http\Controllers\ProductController::class, 'store'])->name('products.store')->middleware('check.permission:products.store');
    Route::get('edit/{id}', [App\Http\Controllers\ProductController::class, 'edit'])->name('products.edit')->middleware('check.permission:products.edit');
    Route::post('update/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('products.update')->middleware('check.permission:products.update');
    Route::post('delete/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('products.destroy')->middleware('check.permission:products.destroy');


});
