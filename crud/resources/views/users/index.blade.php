@extends('home')


@section('content1')
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur"
             navbar-scroll="true">
            <div class="container-fluid py-1 px-3">

                <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                    <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                        <form action="{{ route('users.index') }}" method="get">
                            <div class="input-group input-group-outline">
                                <input type="text" name="name" class="form-control" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" placeholder="name">
                                <input type="text" name="email" class="form-control" value="{{ isset($_GET['email']) ? $_GET['email'] : '' }}" placeholder="email">
                                <select class="form-control" name="role_name" style="text-align: left"
                                        aria-label="Default select example">
                                    <option></option>
                                    @foreach($roles as $r)
                                        <option value="{{$r->name}}" {{ (isset($_GET['role_name']) && ($_GET['role_name'] == $r->name)) ? 'selected' : '' }}>{{$r->name}}</option>
                                    @endforeach
                                </select>

                                <button type="submit" class="btn btn-primary">search</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <h1><p id="ms" class="text-success"></p></h1>

                    <div class="card my-4">

                        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                            @hasPermission('users.create')
                            <button type="button" class="btn btn-primary js-modal-create">
                                Create
                            </button>
                            @endhasPermission
                            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                <h6 class="text-white text-capitalize ps-3">Users Management</h6>
                            </div>
                            <div class="pull-left">

                            </div>

                        </div>

                        <div class="card-body px-0 pb-2">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center mb-0">

                                    <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            id
                                        </th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            name
                                        </th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            email
                                        </th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            role
                                        </th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            action
                                        </th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $key => $user)
                                        <tr>
                                            <td>
                                                <div class="d-flex px-2 py-1">

                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm" id="idUser">{{ $user->id }}</h6>

                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0">{{ $user ->name }}</p>

                                            </td>
                                            <td class="align-middle text-center text-sm">
                                                <span
                                                    class="badge badge-sm bg-gradient-success">{{ $user->email }}</span>
                                            </td>
                                            <td class="align-middle text-center">
                                                <span
                                                    class="text-secondary text-xs font-weight-bold">{{$user->roles[0]->name ??'' }}</span>

                                            </td>
                                            <td class="align-middle">
                                                @hasPermission('users.edit')
                                                <a class="btn btn-warning js-modal-update"
                                                   data-route="{{route('users.edit',['id'=>$user->id])}}">Edit</a>
                                                 @endhasPermission
                                                @hasPermission('users.destroy')
                                                <a class="btn btn-danger js-modal-delete"
                                                   data-route="{{route('users.destroy',['id'=>$user->id])}}">Delete</a>
                                                @endhasPermission
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>
                    {{ $users->links() }}
                </div>

            </div>
    </main>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Create</h4>
                    <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- create -->
                <div class="modal-body">

                    <form action="" method="POST" data-route="{{ route('users.store') }}" id="form-create">
                        @csrf

                        <div class="form-group" style="text-align: left">
                            <label for="email">Name:</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter name">
                            <span class="error-form text-danger"></span>

                        </div>
                        <div class="form-group" style="text-align: left">
                            <label for="email">Email address:</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email">
                            <span class="error-form text-danger"></span>
                        </div>
                        <div class="form-group" style="text-align: left">
                            <label for="pwd">Password:</label>
                            <input type="password" name="password" class="form-control" placeholder="Enter password"
                                   id="pwd">
                            <span class="error-form text-danger"></span>
                        </div>
                        <label for="pwd">Role:</label>
                        <select class="form-select form-group " name="role" style="text-align: left"
                                aria-label="Default select example">

                            @foreach($roles??[] as $r)

                                <option value="{{$r->id}}" >{{$r->name}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary js-btn-create">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>





    <div class="modal fade" id="myModalUpdate">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Edit -->
                <div class="modal-body">
                    @foreach ($users as $key => $user)

                        <form method="POST" data-route="{{route('users.update',['id'=>$user->id])}}" id="form-update">
                            @endforeach
                            @csrf

                            <input type="hidden" id="id" name="id" class="form-control">

                            <div class="form-group" style="text-align: left">
                                <label for="email">Name:</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Enter name">
                                <span class="error-form text-danger"></span>
                            </div>
                            <div class="form-group" style="text-align: left">
                                <label for="email">Email address:</label>
                                <input type="email" name="email" id="email" class="form-control"
                                       placeholder="Enter email" disabled="">
                                <span class="error-form text-danger"></span>
                            </div>
                            <input type="checkbox" id="changePassword" name="changePassword">
                            <label>Change Password</label>
                            <div class="form-group" style="text-align: left">
                                <label for="pwd">Password:</label>
                                <input type="password" name="password" class="form-control password"
                                       placeholder="Enter password" disabled="" id="pwd">
                                <span class="error-form text-danger"></span>
                            </div>
                            <div class="form-group" style="text-align: left">
                                <label> password again</label>
                                <input type="password" name="password_confirmation" class="form-control password"
                                       placeholder="Enter password  again" disabled="" id="pwd">
                                <span class="error-form text-danger"></span>
                            </div>
                            <select class="form-select form-group" name="role" style="text-align: left"
                                    aria-label="Default select example">

                                @foreach($roles??[] as $r)


                                    <option value="{{$r->id}}" >{{$r->name}}</option>

                                @endforeach
                            </select>

                            <button type="submit" class="btn btn-primary js-btn-update">Submit</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <!-- delete -->
    <div class="modal" method="POST" id="myModalDelete" tabindex="-1" role="dialog">
        @csrf
        @method('post')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <p> Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger js-btn-delete">Delete</button>
                    <button type="button" class="btn btn-secondary " id="xx" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(function () {


            $(".js-modal-create").click(function (event) {
                event.preventDefault();
                $("#myModal").modal('show');
                $("#form-create")[0].reset();
                document.getElementById("form-create").reset();
            })
            $(".js-modal-update").click(function (event) {
                event.preventDefault();

                let url = $(this).data('route');
                $.ajax({
                    url: url,
                    method: "GET",
                    dataType: 'json',
                    success: function (response) {
                        // console.log(response);
                        $('#name').val(response.userEdit.name);
                        $('#email').val(response.userEdit.email);
                        $('#id').val(response.userEdit.id);


                    }

                })
                $('#changePassword').change(function () {
                    if ($(this).is(":checked")) {
                        $(".password").removeAttr('disabled');
                    } else {
                        $(".password").attr('disabled', '');
                    }
                });


                $("#myModalUpdate").modal('show');
                $("#form-update")[0].reset();


            });
            $(".js-modal-delete").click(function (event) {
                event.preventDefault();
                $("#myModalDelete").modal('show');
                let url = $(this).data('route');


                $('.js-btn-delete').click(function (e) {
                    e.preventDefault();

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: url,
                        type: 'post',
                        method: "post",
                        success: function () {
                            $("#ms").html("<p> Category has been delete!</p>");
                            window.setTimeout(function () {
                                location.reload()
                            }, 3000)

                        }

                    })


                        .done(function (results) {
                            $("#myModalDelete").modal('hide');


                        })

                        .fail(function (data) {
                            var errors = data.responseJSON;
                            $.each(errors.errors, function (i, val) {
                                $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                            });
                        });
                });
            })


        })

        $(".close").click(function (event) {
            event.preventDefault();
            $("#myModal").modal('hide');
            $("#myModalUpdate").modal('hide');
            $("#myModalDelete").modal('hide');
            location.reload();

        })
        $("#xx").click(function (event) {
            event.preventDefault();
            $("#myModalDelete").modal('hide');


        })


        $('.js-btn-create').click(function (e) {
            e.preventDefault();
            // let url = "{{ route('users.store') }}";
            let url = $("#form-create").data('route');
            //  console.log(url);
            let $this = $(this);
            let $domForm = $this.closest('form');
            $.ajax({
                url: url,
                data: $('#form-create').serialize(),
                method: "POST",
                success: function () {
                    $("#ms").html("<p> users has been add!</p>");
                    window.setTimeout(function () {
                        location.reload()
                    }, 3000)
                }

            })

                .done(function (results) {
                    $("#myModal").modal('hide');
                    $("#form-create")[0].reset();


                })

                .fail(function (data) {
                    var errors = data.responseJSON;
                    $.each(errors.errors, function (i, val) {
                        $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                    });
                });
        });

        $('.js-btn-update').click(function (e) {
            e.preventDefault();


            var id = $("input[name=id]").val();
            var url = '{{ route("users.update", ":id") }}';
            url = url.replace(':id', id);

            let $this = $(this);
            let $domForm = $this.closest('form');


            $.ajax({
                    url: url,

                    data: $('#form-update').serialize(),
                    method: "POST",
                    success: function () {
                        $("#ms").html("<p> User has been update!</p>");
                        window.setTimeout(function () {
                            location.reload()
                        }, 3000)

                    }

                }
            )

                .done(function (results) {
                    $("#myModalUpdate").modal('hide');
                    $("#form-update")[0].reset();

                })
                .fail(function (data) {
                    var errors = data.responseJSON;
                    $.each(errors.errors, function (i, val) {
                        $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                    });
                });

        });
    </script>

@endsection
