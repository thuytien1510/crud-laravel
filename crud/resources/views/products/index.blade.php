@extends('home')


@section('content1')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">

            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <form action="{{ route('products.index') }}" method="get">
                        <div class="input-group input-group-outline">
                            <input type="text" name="name" class="form-control"  value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" placeholder="name">
                            <input type="text" name="summary" class="form-control" value="{{ isset($_GET['summary']) ? $_GET['summary'] : '' }}" placeholder="summary">
                            <select class="form-control" name="category_name" style="text-align: left" aria-label="Default select example">

                            <option></option>
                               @foreach($category as $key => $p)
                                <option value="{{ $p->name }}" {{ (isset($_GET['category_name']) && ($_GET['category_name'] == $p->name)) ? 'selected' : '' }}>{{$p->name}}</option>
                                @endforeach


                            </select>

                            <button type="submit" class="btn btn-primary">search</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <h1><p id="ms" class="text-success"></p></h1>
                <div class="card my-4">

                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        @hasPermission('products.create')
                        <button type="button" class="btn btn-primary js-modal-create">
                            Create
                        </button>
                        @endhasPermission
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">product Management</h6>
                        </div>
                        <div class="pull-left">

                        </div>

                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">id</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">name</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">slug</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">summary</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">image </th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">category</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">action</th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products??[] as $key => $d)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">

                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm" id="idUser">{{ $d->id }}</h6>

                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $d ->name }}</p>

                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <span class="badge badge-sm bg-gradient-success">{{ $d->slug }}</span>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <span class="badge badge-sm bg-gradient-success">{{ $d->summary }}</span>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <span class="badge badge-sm bg-gradient-success"><img src="{{$d->image}}" width="150px" class="css-class" alt="alt text"> </span>
                                        </td>
                                        <td class="align-middle text-center">
                                            <span class="text-secondary text-xs font-weight-bold">{{$d->category[0]->name ??'' }}</span>

                                        </td>
                                        <td class="align-middle">
                                            @hasPermission('products.edit')
                                            <a class="btn btn-warning js-modal-update" data-route="{{route('products.edit',['id'=>$d->id])}}">Edit</a>
                                             @endhasPermission
                                            @hasPermission('products.destroy')
                                            <a class="btn btn-danger js-modal-delete" data-route="{{route('products.destroy',['id'=>$d->id])}}">Delete</a>
                                            @endhasPermission
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
                {{ $products->links() }}
            </div>

        </div>
</main>
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create</h4>
                <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- create -->
            <div class="modal-body">

                <form action="" method="POST" data-route="{{ route('products.store') }}" enctype="multipart/form-data" id="form-create">
                    @csrf

                    <div class="form-group" style="text-align: left">
                        <label for="email">Name:</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name">


                        <span class="error-form text-danger"></span>

                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">Summary:</label>
                        <input type="text" name="summary" class="form-control" placeholder="Enter summary">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">content:</label>
                        <input type="text" name="content" class="form-control" placeholder="Enter content">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="pwd">image:</label>
                        <input type="file" id="image" name="image" class="form-control">
                        <span class="error-form text-danger"></span>
                    </div>
                    <label for="pwd">Category:</label>

                    <div class="form-group">
                        <label>parent</label>

                        <select class="form-select form-group" name="parent_id">

                            @foreach($parent as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @if($c->childs)
                                    @foreach($c->childs as $cc)
                                        <option value="{{$cc->id}}">--{{$cc->name}}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary js-btn-create">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>




<!--edit -->
<div class="modal fade" id="myModalUpdate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">


            <div class="modal-header">
                <h4 class="modal-title">Edit</h4>
                <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body">
                @foreach ($products??'' as $key => $d)
                <form method="POST" data-route="{{route('products.update',['id'=>$d->id])}}" id="form-update">
                    @endforeach
                    @csrf

                    <input type="hidden" id="id" name="id" class="form-control">

                    <div class="form-group" style="text-align: left">
                        <label for="email">Name:</label>
                        <input type="text" name="name" id="name"  class="form-control" placeholder="Enter name">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">Summary:</label>
                        <input type="text" name="summary" id="summary"  class="form-control" placeholder="Enter summary">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">content:</label>
                        <input type="text" name="content" id="content"  class="form-control" placeholder="Enter content">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="pwd">image:</label>
                        <input type="file" id="image" name="image"  class="form-control">
                        <span class="error-form text-danger"></span>
                    </div>
                    <label for="pwd">Category:</label>
                    <select class="form-select form-group" name="parent_id">
                      <option value="0">--Select--</option>
                        @foreach($parent as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                            @if($c->childs)
                                @foreach($c->childs as $cc)
                                    <option value="{{$cc->id}}">--{{$cc->name}}</option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>

                    <button type="submit" class="btn btn-primary js-btn-update">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- delete -->
<div class="modal" method="POST" id="myModalDelete" tabindex="-1" role="dialog">
    @csrf
    @method('post')

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <p> Are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger js-btn-delete">Delete</button>
                <button type="button" class="btn btn-secondary" id="xx" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
    $(function() {


        $(".js-modal-create").click(function(event) {
            event.preventDefault();
            $("#myModal").modal('show');
            $("#form-create")[0].reset();

        })
        $(".js-modal-update").click(function(event) {
            event.preventDefault();

            let url = $(this).data('route');
            $.ajax({
                url: url,
                method: "GET",
                dataType: 'json',
                success: function(response) {
                    // console.log(response);
                    $('#name').val(response.productEdit.name);
                    $('#summary').val(response.productEdit.summary);
                    $('#content').val(response.productEdit.content);
                    $('#id').val(response.productEdit.id);

                }
            })
            $("#myModalUpdate").modal('show');
            $("#form-update")[0].reset();



        });
        $(".js-modal-delete").click(function(event) {
            event.preventDefault();
            $("#myModalDelete").modal('show');
            let url = $(this).data('route');


            $('.js-btn-delete').click(function(e) {
                e.preventDefault();

                $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: url,
                        type: 'post',
                        method: "post",
                        success: function() {
                            $("#ms").html("<p> product has been delete!</p>");
                            window.setTimeout(function () {
                                location.reload()
                            }, 3000)
                        }

                    })


                    .done(function(results) {
                        $("#myModalDelete").modal('hide');


                    })

                    .fail(function(data) {
                        var errors = data.responseJSON;
                        $.each(errors.errors, function(i, val) {
                            $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                        });
                    });
            });
        })



    })

    $(".close").click(function(event) {
        event.preventDefault();
        $("#myModal").modal('hide');
        $("#myModalUpdate").modal('hide');
        location.reload();
    })
    $("#xx").click(function(event) {
        event.preventDefault();
        $("#myModalDelete").modal('hide');


    })


    $('.js-btn-create').click(function(e) {
        e.preventDefault();
        let url = $("#form-create").data('route');
        var data = new FormData(document.getElementById("form-create"));

        let $this = $(this);
        let $domForm = $this.closest('form');
        $.ajax({

                type: 'POST',

                url: url,

                data: data,

                cache: false,

                contentType: false,

                processData: false,
            })

            .done(function(results) {
                $("#myModal").modal('hide');
                $("#form-create")[0].reset();
                $("#ms").html("<p> product has been create!</p>");
                window.setTimeout(function () {
                    location.reload()
                }, 3000)

            })
            .fail(function(data) {
                var errors = data.responseJSON;
                $.each(errors.errors, function(i, val) {
                    $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                });
            });



    });

    $('.js-btn-update').click(function(e) {
        e.preventDefault();

        let $this = $(this);
        let $domForm = $this.closest('form');
        var id = $("input[name=id]").val();
        var url = '{{ route("products.update", ":id") }}';
        url = url.replace(':id', id);

        var data = new FormData(document.getElementById("form-update"));

        $.ajax({

                type: 'POST',

                url: url,

                data: data,

                cache: false,

                contentType: false,

                processData: false,
            })

            .done(function(results) {
                $("#myModalUpdate").modal('hide');
                $("#form-update")[0].reset();
                $("#ms").html("<p> products has been update!</p>");
                window.setTimeout(function () {
                    location.reload()
                }, 3000)
            })
            .fail(function(data) {
                var errors = data.responseJSON;
                $.each(errors.errors, function(i, val) {
                    $domForm.find('input[name=' + i + ']').siblings('.error-form').text(val[0]);
                });
            });


    });
</script>

@endsection
