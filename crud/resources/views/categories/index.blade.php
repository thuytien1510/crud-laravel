    @extends('home')


@section('content1')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">

            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                <form action="{{ route('categories.index') }}" method="get">
                <div class="input-group input-group-outline">

                        <input type="text" name="category_name"  value="{{ isset($_GET['category_name']) ? $_GET['category_name'] : '' }}" placeholder="name" class="form-control">
                    </div>
                </form>
                </div>

            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        @if(session('messenger'))
            <div class="alert alert-success">
                {{session('messenger')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        @endif
        <div class="row">
            <div class="col-12">
                <div class="card my-4">

                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        @hasPermission('categories.create')
                    <a href="{{route('categories.create')}}">  <button type="button" class="btn btn-primary ">
                            Create

                        </button></a>
                        @endhasPermission
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Categories Management</h6>
                        </div>
                        <div class="pull-left">

                        </div>

                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">id</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">name</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">slug</th>

                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">action</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($categories as $key => $category)
                                    <tr>
                                        <td>
                                            <div class="d-flex px-2 py-1">

                                                <div class="d-flex flex-column justify-content-center">
                                                    <h6 class="mb-0 text-sm">{{ $category->id }}</h6>

                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $category->name }}</p>

                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $category->slug }}</p>

                                        </td>


                                        <td class="align-middle">
                                            @hasPermission('categories.edit')
                                            <a class="btn btn-warning " href="{{route('categories.edit',['id'=>$category->id])}}">Edit</a>
                                            @endhasPermission
                                            @hasPermission('categories.destroy')
                                            <a class="btn btn-danger " href="{{route('categories.destroy',['id'=>$category->id])}}" onclick="return confirm('are you sure ?');">Delete</a>
                                             @endhasPermission
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
                {{ $categories->links() }}
                </div>

            </div>
    </main>


    @endsection
