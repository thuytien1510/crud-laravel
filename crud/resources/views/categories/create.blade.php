@extends('home')


@section('content1')

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">

            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                    <div class="input-group input-group-outline">
                        <label class="form-label">Type here...</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">

                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <!-- <button type="button" class="btn btn-primary ">
                            <a href="{{route('roles.create')}}"> Create</a>

                        </button> -->
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Roles Management</h6>
                        </div>
                        <div class="pull-left">

                        </div>

                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">

                            <form action="{{ route('categories.store') }}" method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>name</label>
                                        <input type="text" class="form-control" name="name"   value="{{old('name')}}"  placeholder="Enter  name">
                                        <div class="text-danger">  {{$errors->first('name')}}</div>
                                    </div>
                                    <div class="form-group">


                                    </div>
                                    <div class="form-group">
                                        <label>parent</label>
                                        <select class="form-select form-group" name="parent_id">
                                            <option value="0">--Select--</option>
                                            @foreach($categories as $c)
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                            @if($c->childs)
                                            @foreach($c->childs as $cc)
                                            <option value="{{$cc->id}}">--{{$cc->name}}</option>
                                            @endforeach
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>








                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer ">
                                    <button type="submit" class="btn btn-primary"> Create</button>
                                    <button type="reset" class="btn btn-danger">reset </button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </div>
</main>

@endsection
