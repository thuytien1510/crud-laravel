<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Role;

use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_has_roles');
    }

    /**
     * Scope a query to search user by first name.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithName($query, $Name)
    {
        return $Name ? $query->where('name', 'LIKE', '%' . $Name . '%') : null;
    }

    /**
     * Scope a query to search user by email.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', 'LIKE', '%' . $email . '%') : null;
    }

    /**
     * Scope a query to search user by role.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithRoleName($query, $roleName)
    {
        return $roleName ? $query->whereHas('roles',
            fn($query) => $query->where('name', 'LIKE', '%' . $roleName . '%')) : null;
    }



    // public function scopeSearch($query, $request)
    // {


    //     if (isset($request->name)) {
    //         $query->where('name', 'LIKE', '%' . $request->name . '%');
    //     }

    //     if (isset($request->email)) {
    //          $query->where('email', 'LIKE', '%' . $request->email . '%');

    //     }

    //     if (isset($request->role_name)) {

    //         //$query  = User::with(['roles' => fn ($query) => $query->where('name', '=', $request->role_name)])
    //         // ->whereHas('roles', fn ($query) =>  $query->where('name', '=', $request->role_name));

    //          $query->where('name', 'LIKE', '%' . $request->role_name . '%')
    //             ->orWhereHas('roles', function ($query) use($request){
    //                 $query->where('name', 'LIKE', '%' . $request->role_name . '%');
    //             });
    //     };
    // }

    public function hasRole($roleName)
    {

        foreach ($this->roles as $role) {
            if ($role->name == $roleName) {

                return true;
            }
        }
        return false;

    }

    public function hasPermission($permissionName)
    {


        $listRole = $this->roles;
        foreach ($listRole as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }
        return false;

    }


}
