<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table ="roles";

    protected $fillable = ['name'];
    public function user()
    {
        return $this->belongsToMany(User::class,'user_has_roles');
    }
     public function permissions()
     {
         return $this->belongsToMany(Permission::class,'role_has_permissions');
     }


    public function scopeWithName($query, $role_name)
    {
        return $query->where('name', 'LIKE', '%' . $role_name . '%');
    }

    public function hasPermission($permissionName)
    {
      //return $this->permissions->contains('name',)

        foreach ($this->permissions as $permission) {
            if ($permission->name == $permissionName) {
                return true;
            }
        }
        return false;
    }

}



