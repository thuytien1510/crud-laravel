<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $fillable = ['name', 'slug', 'summary', 'content', 'image'];
    public $dir ='upload/products/';
    public function getImageAttribute($value)
    {
        return  $this->dir. $value;
    }
    public function  category()
    {
        return  $this->belongsToMany(Category::class, 'category_has_products', 'product_id', 'category_id');
    }

  /**
     * Scope a query to search products by name.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithName($query, $name)
    {
        return  $query->where('name', 'LIKE', '%' . $name . '%') ;
    }

    /**
     * Scope a query to search products by price.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithSummary($query, $summary)
    {
        return  $query->where('summary', 'LIKE', '%' . $summary . '%') ;
    }

    /**
     * Scope a query to search products by category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCategoryName($query, $categoryName)
    {
        return $query->whereHas('category', fn ($query) =>
                                 $query->where('name', 'LIKE', '%' . $categoryName . '%'));
    }
}
