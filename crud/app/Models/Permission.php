<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $table ="permissions";
  public function roles()
  {
      return $this->belongsToMany(Role::class,'user_has_roles');
  }


//    public function hasPermission($permissionName)
//    {
//        dd(Permission::all());
//          foreach (Permission::all() as $permission) {
//            if ($permission->name == ($permissionName)) {
//                return true;
//            }
//        }
//
//        return false;
//    }
}
