<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;


trait HandleImage
{
    protected $path = 'upload/products';


    public function verifyImage($request): bool
    {
        if ($request->file('image')) {
            return true;
        }
        return false;
    }

    public function storeImage($request)
    {

        if ($this->verifyImage($request)) {
            $image = $request->image;
            $imageExt = $image->getClientOriginalExtension();
            $name = time() . '.' . $imageExt;
            $image->move($this->path, $name);
            return $name;
        }

    }

    public function deleteImage($image)
    {


        if (file_exists($image)) {
            unlink($image);
        }
    }

    public function updateImage($request, $currentImage)
    {

        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->storeImage($request);
        }
        return $currentImage;
    }
}
