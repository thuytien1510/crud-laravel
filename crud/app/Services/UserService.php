<?php

namespace App\Services;

use App\Models\User_has_Role;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class UserService
{
    /**
     * @var $userRepository
     */
    protected $userRepository;

    /**
     * PostService constructor.
     *
     * @param UserRepository $userRepository ;
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get All post
     * @return String
     */
    public function all()
    {
        return $this->userRepository->all();
    }

    public function getAll()
    {
        return $this->userRepository->getAllDesc();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['role_name'] = $request->role_name ?? null;
        $dataSearch['email'] = $request->email ?? null;
        return $this->userRepository->search($dataSearch);
    }

    public function create($request)
    {


        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $user = $this->userRepository->create($data);
        $idRole = $request->role;
        $this->userRepository->createUserHasRoles($user->id, $idRole);

    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function update($request, $id)
    {

        $data['name'] = $request->name;

        if (isset($request->password)) {
            $data['password'] = Hash::make($request->password);
        }

        $user = $this->userRepository->update($data, $id);
        $idRole = $request->role;
        $this->userRepository->updateUserHasRoles($user->id,$idRole);

    }

    public function destroy($id)
    {
        return $this->userRepository->delete($id);
    }
}

