<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService
{
    /**
     * @var $permissionRepository
     */
    protected $permissionRepository;

    /**
     * PermissionService constructor.
     *
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(
        PermissionRepository $permissionRepository
    ) {
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->permissionRepository->all();
    }
}
