<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Models\Role;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function getAllDesc()
    {
        return $this->model->orderBy('id', 'DESC')->paginate(5);
    }

    public function getById($id)
    {

        return $this->model->findOrFail($id);
    }

    public function search($dataSearch)
    {

        return $this->model->withName($dataSearch['role_name'])->paginate(5);
    }

    public function createRoleHasPermissions($permissions, $idRole)
    {
        foreach ($permissions as $value) {
            DB::table('role_has_permissions')->insert([
                'permission_id' => $value,
                'role_id' => $idRole,
            ]);
        }
    }

    public function updateRoleHasPermissions($permissions, $idRole)
    {
        DB::table('role_has_permissions')->where('role_id', $idRole)->delete();
        foreach ($permissions as $value) {
            DB::table('role_has_permissions')->insert([
                'permission_id' => $value,
                'role_id' => $idRole,

            ]);
        }
    }

    public function getRolePermissions($id)
    {

        return DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
    }




}
