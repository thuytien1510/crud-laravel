<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function getAllDesc()
    {
        return $this->model->orderBy('id', 'DESC')->paginate(5);
    }

    public function getById($id)
    {

        return $this->model->findOrFail($id);
    }

    public function createUserHasRoles($id, $idRole)
    {
        DB::table('user_has_roles')->insert([
            'role_id' => $idRole,
            'user_id' => $id,

        ]);
    }

    public function updateUserHasRoles($id, $idRole)
    {
        DB::table('user_has_roles')->where('user_id', $id)->delete();
        DB::table('user_has_roles')->insert([
            'user_id' => $id,
            'role_id' => $idRole
        ]);
    }

    public function search($dataSearch)
    {

        return $this->model->withRoleName($dataSearch['role_name'])->withName($dataSearch['name'])
            ->withEmail($dataSearch['email'])->latest()->paginate(5);
    }
}
