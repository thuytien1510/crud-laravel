<?php

namespace App\Repositories;

use App\Models\Product;


use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return product::class;
    }

    public function getAllDesc()
    {
        return $this->model->latest()->paginate(5);
    }

    public function createCategoryHasProducts($id, $idProduct)
    {
        DB::table('category_has_products')->insert(
            [
                'category_id' => $id,
                'product_id' => $idProduct
            ]
        );
    }

    public function updateCategoryHasProducts($id, $idProducts)
    {

        DB::table('category_has_products')->where('product_id', $idProducts)->delete();
        DB::table('category_has_products')->insert([
            'category_id' => $id,
            'product_id' => $idProducts
        ]);
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->withSummary($dataSearch['summary'])
            ->withCategoryName($dataSearch['category_name'])->paginate(5);
    }

    //lien quan db


}
