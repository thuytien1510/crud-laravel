<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Permission;

use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function getAllDesc()
    {
        return $this->model->latest()->paginate(5);
    }


    public function search($dataSearch)
    {

        return $this->model->withName($dataSearch['category_name'])->latest()->paginate(5);
    }

    public function getCategoryParent()
    {
        return $this->model->where('parent_id', 0)->latest()->get();
    }
    public function  createParent($id,$parentId)
    {
        return $this->model->where('id',$id)->update(['parent_id'=>$parentId]);
    }
    public  function updateParent($id, $parentId)
    {
        return $this->model->where('id',$id)->update(['parent_id'=>$parentId]);
    }


    //lien quan db


}
