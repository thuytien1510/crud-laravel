<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        // return Category::with('childs')->where('parent_id',0)->get();
        //  $category = Category::where('parent_id',0)->get();
        $categories = $this->categoryService->getCategoryParent();
        return view('categories.create', compact('categories'));
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect(route('categories.index'))->with('messenger', 'create Success !');
    }

    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        $categoryParent = $this->categoryService->getCategoryParent();
        return view('categories.edit', ['category' => $category, 'categoryParent' => $categoryParent]);
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return redirect(route('categories.index'))->with('messenger', 'update Success !');
    }

    public function destroy($id)
    {
        $this->categoryService->destroy($id);
        return redirect(route('categories.index'))->with('messenger', 'delete Success !');
    }
}
