<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;

use App\Services\UserService;
use App\Services\RoleService;
use Exception;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var $userService
     * @var $roleService
     */
    protected $userService;
    protected $roleService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     * @param RoleService $roleService
     */
    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        //  $user = User::with('roles')->first(); //get() lay het //first lay cai dau tien
        //  return  $user->roles;
         $users = $this->userService->search($request);
        $roles = $this->roleService->all();
        return view('users.index', compact('users', 'roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $this->userService->create($request);
        return redirect(route('users.index'));
    }

    public function edit($id)
    {
        $userEdit = $this->userService->findOrFail($id);
        return response()->json(['userEdit' => $userEdit]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect(route('users.index'));
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect(route('users.index'));
    }
}
