<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class adminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
//    if(Auth::user()->hasRole
         if (Auth::check()) {
           // dd(Auth::user()->user_has_role);
             if (Auth::user()->user_has_role) {
                 $routename = Route::currentRouteName();
                 $user = Auth::user();
                 $user = User::with('roles')->first();
                 $id = $user->roles[0]->id;
                 $per = Permission::query()
                     ->join('role_has_permissions', 'permissions.id', 'role_has_permissions.permission_id')
                     ->join('roles', 'roles.id', 'role_has_permissions.role_id')
                     ->where('role_id', $id)
                     ->select('permissions.*', 'role_has_permissions.*')
                     ->pluck('name')->toArray();
                 // dd($per,$routename);
                 if (in_array($routename, $per)) {
                     return $next($request);
                 }
                 return abort(404);
             }
             return abort(404);
         }
         return abort(404);
    }
}
