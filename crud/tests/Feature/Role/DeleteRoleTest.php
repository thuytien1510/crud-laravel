<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    use WithoutMiddleware;
    public function getDestroyRoleRoute($id)
    {
        return route('roles.destroy', $id);
    }
    public function getRedirectRoleRoute()
    {
        return route('roles.index');
    }

    /** @test*/

    public function authenticate_can_destroy_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->create();

        $roleBefore = Role::count();
        $response = $this->get($this->getDestroyRoleRoute($role->id));
        $roleAfter = Role::count();
        $this->assertEquals($roleBefore - 1, $roleAfter);
        $this->assertDatabaseMissing('roles', $role->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }
    /** @test*/
    public function authenticate_can_not_destroy_role()
    {
        $id = -1;

        $response = $this->get($this->getDestroyRoleRoute($id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
