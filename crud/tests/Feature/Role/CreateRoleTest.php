<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    use WithoutMiddleware;

    public function getCreateRoleRoute()
    {
        return route('roles.store');
    }

    public function getRedirectRoleRoute()
    {
        return route('roles.index');
    }

    /**
     * @test
     */
    public function authenticate_can_store_role()
    {
        $this->actingAs(User::factory()->create());
        $data = [
            'name' => $this->faker->name,
            'permission' => array(1, 2, 3)
        ];

        $roleAfter = Role::count();
        $response = $this->post($this->getCreateRoleRoute(), $data);
        $roleBefore = Role::count();
        $this->assertEquals($roleAfter + 1, $roleBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }

    /**
     * @test
     */
    public function authenticate_can_not_store_role_if_name_null()
    {
        $this->actingAs(User::factory()->create());
        $data = [
            'name' => '',
            'permission' => array(1, 2, 3)
        ];
        $response = $this->post($this->getCreateRoleRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticate_can_not_store_role_if_permission_null()
    {
        $this->actingAs(User::factory()->create());
        $data = [
            'name' => $this->faker->name,
            'permission' => ''
        ];
        $response = $this->post($this->getCreateRoleRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['permission']);
    }


}
