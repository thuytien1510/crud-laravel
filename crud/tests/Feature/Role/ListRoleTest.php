<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListRoleTest extends TestCase
{

    public function getListRoleRoute()
    {
        return route('roles.index');
    }
    /**
     @test
     */
    public function authenticate_can_get_list_role()
    {

        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);


    }


    /**
     * @test
     */
    public function unauthenticated_can_not_get_list_role()
    {


        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }


}
