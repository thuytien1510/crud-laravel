<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditProductTest extends TestCase
{
    use WithoutMiddleware;

    public function getEditProductRoute($id)
    {
        return route('products.update', $id);
    }

    public function getRedirecProductRoute()
    {
        return route('products.index');
    }

    /**
     * @test
     */
    public function authenticate_can_update_product()
    {
        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();
        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->name,
            'summary' => $this->faker->name,
            'content' => $this->faker->text,
            'image' => $this->faker->image,
            'parent_id' => 0
        ];

        $response = $this->post($this->getEditProductRoute($product->id), $data);
        $productCheck = Product::find($product->id);
        $this->assertSame($productCheck->name, $data['name']);
        $this->assertSame($productCheck->summary, $data['summary']);
        $this->assertSame($productCheck->content, $data['content']);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirecProductRoute());

    }

    /** @test */

    public function authenticate_can_not_edit_product_if_name_is_null()
    {

        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();


        $data = [
            'name' => '',
            'slug' => $this->faker->name,
            'summary' => $this->faker->name,
            'content' => $this->faker->text,
            'image' => $this->faker->image,
            'parent_id' => 0
        ];

        $response = $this->post($this->getEditProductRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */

    public function authenticate_can_not_edit_product_if_slug_is_null()
    {

        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();


        $data = [
            'name' => $this->faker->name,
            'slug' => '',
            'summary' => $this->faker->name,
            'content' => $this->faker->text,
            'image' => $this->faker->image,
            'parent_id' => 0
        ];

        $response = $this->post($this->getEditProductRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['slug']);
    }

    /** @test */

    public function authenticate_can_not_edit_product_if_summary_is_null()
    {

        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();


        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->name,
            'summary' => '',
            'content' => $this->faker->text,
            'image' => $this->faker->image,
            'parent_id' => 0
        ];

        $response = $this->post($this->getEditProductRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['summary']);
    }

    /** @test */

    public function authenticate_can_not_edit_product_if_content_is_null()
    {

        $this->actingAs(User::factory()->create());
        $product = Product::factory()->create();


        $data = [
            'name' => $this->faker->name,
            'slug' => $this->faker->name,
            'summary' => $this->faker->name,
            'content' => '',
            'image' => $this->faker->image,
            'parent_id' => 0
        ];

        $response = $this->post($this->getEditProductRoute($product->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content']);
    }

}
