<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateProductTest extends TestCase
{

    use WithoutMiddleware;

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function getRedirectProductRoute()
    {
        return route('products.index');
    }

//    /**
//     * @test
//     */
//    public function authentica_can_store_product()
//    {
//        $this->actingAs(User::factory()->create());
//
//        $data = [
//            'name'=> $this->faker->name,
//            'slug'=> $this->faker->name,
//            'summary'=>$this->faker->name,
//            'content'=>$this->faker->text,
//            'image'=> $this->faker->image,
//            'parent_id'=>45
//        ];
//        $productAfter = Product::count();
//        $response = $this->post($this->getStoreProductRoute(),$data);
//        $productBefore = Product::count();
//        $this->assertEquals($productAfter + 1, $productBefore);
//        $response->assertStatus(Response::HTTP_FOUND);
//        $response->assertRedirect($this->getRedirectProductRoute());
//
//    }

    /**
     * @test
     */
    public function authentica_can_not_store_product_if_name_null()
    {
        $this->actingAs(User::factory()->create());
        $data = Product::factory()->make(['name' => ''])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);

    }

    /**
     * @test
     */
    public function authentica_can_not_store_product_if_summary_null()
    {
        $this->actingAs(User::factory()->create());
        $data = Product::factory()->make(['summary' => ''])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['summary']);

    }

    /**
     * @test
     */
    public function authentica_can_not_store_product_if_image_null()
    {
                $data = [
            'name'=> $this->faker->name,
            'slug'=> $this->faker->name,
            'summary'=>$this->faker->name,
            'content'=>$this->faker->text,
            'image'=> '',
            'parent_id'=>45
        ];
        $response = $this->post($this->getStoreProductRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['image']);

    }


}
