<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListProductTest extends TestCase
{


    public function getListProductRoute()
    {
        return route('products.index');
    }

    /**
     * @test
     */
    public function user_can_get_list_product()
    {


        $data = Product::factory()->create();
        $response = $this->get($this->getListProductRoute());

        $response->assertStatus(Response::HTTP_FOUND);


    }


    /**
     * @test
     */
    public function unauthenticated_can_not_get_list_role()
    {
        $user = Product::factory()->create();
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
