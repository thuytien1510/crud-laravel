<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{

    use WithoutMiddleware;

    public function getDeleteProductRoute($id)
    {
        return route('products.destroy', $id);
    }

    /**
     * @test
     */
    public function authenticate_can_delete_product()
    {
        $this->actingAs(User::factory()->create());
        $Product = Product::factory()->create();
        $countProductBefore = Product::count();
        $response = $this->post($this->getDeleteProductRoute($Product->id));
        $countProductAfter = Product::count();
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertEquals($countProductBefore - 1, $countProductAfter);
        $this->assertDatabaseMissing('products', $Product->toArray());
    }


    /** @test */

    public function authenticate_can_not_delete_product()
    {
        $id = -1;
        $response = $this->post($this->getDeleteProductRoute($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}

