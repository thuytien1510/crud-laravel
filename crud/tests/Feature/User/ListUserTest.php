<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;


class ListUserTest extends TestCase
{

    public function getListUserRoute()
    {
        return route('users.index');
    }
    /**
     * @test
     */
    public function user_can_get_list_user()
    {

        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);



    }
     /**
     * @test
     */
    public function unanthenticate_can_get_list_user()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }


}
