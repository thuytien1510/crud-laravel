<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeletetUserTest extends TestCase
{
   use  WithoutMiddleware;
    public function getDestroyUserRoute($id)
    {
        return route('users.destroy', $id);
    }
    public function getRedirectUserRoute()
    {
        return route('users.index');
    }

    /** @test*/

    public function authenticate_can_delete_user()
    {

        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();

        $userBefore = User::count();
        $response = $this->post($this->getDestroyUserRoute($user->id));
        $userAfter = User::count();
        $this->assertEquals($userBefore - 1, $userAfter);
        $this->assertDatabaseMissing('users', $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectUserRoute());
    }
    /** @test*/
    public function authenticate_can_not_delete_user()
    {
        $id = -1;

        $response = $this->post($this->getDestroyUserRoute($id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
