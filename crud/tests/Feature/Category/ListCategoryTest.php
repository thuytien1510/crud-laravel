<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class ListCategoryTest extends TestCase
{

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    /**
     * @test
     */
    public function authenticate_can_get_list_category()
    {



        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);


    }

    /**
     * @test
     */
    public function unauthenticated_can_get_list_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

}
