<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class EditCategoryTest extends TestCase
{
    use WithoutMiddleware;

    public function getEditCategoryRoute($id)
    {
        return route('categories.update', $id);
    }

    public function getRedirectCategoryRoute()
    {
        return route('categories.index');
    }

    /** @test */

    public function authenticate_can_edit_category()
    {

        $this->actingAs(User::factory()->create());

        $category = Category::factory()->create();

        $data = [
            'name' => $this->faker->name(),
            'parent_id' => 0

        ];

        $response = $this->post($this->getEditCategoryRoute($category->id), $data);

        $categoryCheck = Category::find($category->id);
        $this->assertSame($categoryCheck->name, $data['name']);
        $this->assertSame($categoryCheck->parent_id, $data['parent_id']);
        $response->assertRedirect($this->getRedirectCategoryRoute());
    }

    /** @test */

    public function authenticate_can_not_edit_user_if_name_is_null()
    {

        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();


        $data = [
            'name' => '',
            'parent_id' => 0

        ];

        $response = $this->post($this->getEditCategoryRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


}
