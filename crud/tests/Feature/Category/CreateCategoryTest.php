<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateCategoryTest extends TestCase

    {

        use WithoutMiddleware;
        public function getStoreCategoryRoute()
        {
            return route('categories.store');
        }
        public function getRedirectCategoryRoute()
        {
             return route('categories.index');
        }

        /** @test */

        public function authenticate_can_store_category()
        {
            // $this->withoutExceptionHandling();
            $this->actingAs(User::factory()->create());
            $data = Category::factory()->make()->toArray();
            $categoryAfter = Category::count();
            $response = $this->post($this->getStoreCategoryRoute(), $data);
            $categoryBefore = Category::count();
            $this->assertEquals($categoryAfter + 1, $categoryBefore);
            $response->assertStatus(Response::HTTP_FOUND);
            $response->assertRedirect($this->getRedirectCategoryRoute());
        }
        /** @test */
        public function authenticate_can_not_store_category_if_name_is_null()
        {
            $this->actingAs(User::factory()->create());
            $data = Category::factory()->make(['name'=>''])->toArray();


            $response = $this->post($this->getStoreCategoryRoute(), $data);
            $response->assertStatus(Response::HTTP_FOUND);
            $response->assertSessionHasErrors(['name']);

        }



    }
